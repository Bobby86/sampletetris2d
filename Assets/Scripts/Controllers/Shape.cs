﻿using System.Collections;
using UnityEngine;

namespace Controllers
{
	public class Shape : MonoBehaviour 
	{
		#region References
		public delegate void ShapeEvents ();
		public static event ShapeEvents GameOverEvent;

		private float fallDelay;
		private float lastFall;
		private Grid gridScript;
		private Vector3 leftStep = new Vector3 (-1f, 0f, 0f);
		private Vector3 rightStep = new Vector3 (1f, 0f, 0f);
		private Vector3 downStep = new Vector3 (0f, -1f, 0f);
		private Vector3 backUp = new Vector3 (0f, 1f, 0f);
		#endregion

		#region nity Callbacks
		void OnEnable ()
		{
			GlobalDataController.DifficultyUp += DifficultyUpTrigger;
		}

		void OnDisable ()
		{
			GlobalDataController.DifficultyUp -= DifficultyUpTrigger;
		}

		void Start ()
		{
			gridScript = Grid.Instance;
			fallDelay = GlobalDataController.Instance.GetFallDelay ();

			if (!IsValidGridPosition ())
			{
				if (GameOverEvent != null)
				{
					GameOverEvent ();
				}
				Destroy (gameObject);
			}
		}

		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.LeftArrow))
			{
				transform.position += leftStep;

				if (IsValidGridPosition ())
				{
					UpdateGrid (); // leave it at new position
				}
				else // get back
				{
					transform.position += rightStep;
				}
			}
			else if (Input.GetKeyDown (KeyCode.RightArrow))
			{
				transform.position += rightStep;

				if (IsValidGridPosition ())
				{
					UpdateGrid (); // leave it at new position
				}
				else // get back
				{
					transform.position += leftStep;
				}
			}
			else if (Input.GetKeyDown (KeyCode.UpArrow))
			{
				transform.Rotate(0f, 0f, -90f);

				if (IsValidGridPosition ())
				{
					UpdateGrid (); // leave it at new position
				}
				else // get back
				{
					transform.Rotate(0f, 0f, 90f);
				}
			}
			else if (Input.GetKeyDown (KeyCode.DownArrow) || Time.time - lastFall > fallDelay)
			{
				transform.position += downStep;

				if (IsValidGridPosition ())
				{
					UpdateGrid ();
				}
				else
				{
					transform.position += backUp;
					gridScript.DeleteFullRows ();
					Spawner.Instance.SpawnNewShape ();
					enabled = false;
				}
				lastFall = Time.time;
			}
		}
		#endregion // unity callbacks

		#region Helpers
		private bool IsValidGridPosition ()
		{
			foreach (Transform child in transform)
			{
				Vector2 v = gridScript.RoundVector2 (child.position);
				Transform[,] grid = gridScript.GetGrid ();

				if (!gridScript.IsInsideBorder (v))
				{
					return false;
				}

				if (grid[(int)v.x, (int)v.y] != null &&
					grid[(int)v.x, (int)v.y].parent != transform)
				{
					return false;
				}
			}
			return true;
		}

		private void UpdateGrid ()
		{
			var grid = gridScript.GetGrid ();
			// remove old children from grid
			for (int y = 0; y < gridScript.GetHeight (); ++y)
			{
				for (int x = 0; x < gridScript.GetWidth (); ++x)
				{
					if (grid[x, y] != null)
					{
						if (grid[x, y].parent == transform)
						{
							grid [x, y] = null;
						}
					}
				}
			}

			// add new children to a grid
			foreach (Transform child in transform)
			{
				Vector2 v = gridScript.RoundVector2 (child.position);
				grid [(int)v.x, (int)v.y] = child;
			}
		}
		#endregion // helpers

		#region Events and triggers
		private void DifficultyUpTrigger ()
		{
			fallDelay = GlobalDataController.Instance.GetFallDelay ();
		}
		#endregion // Events and triggers
	}
}