﻿using System.Collections;
using UnityEngine;

namespace Controllers
{
	public class Spawner : MonoBehaviour 
	{
		public static Spawner Instance;
		[SerializeField]GameObject[] shapes;

		void Awake ()
		{
			Instance = this;
		}

		void Start ()
		{
			Random.InitState (System.DateTime.Now.Millisecond);
			SpawnNewShape ();
		}

		/// <summary>
		/// Spawns a random shape.
		/// </summary>
		public void SpawnNewShape ()
		{
			int randomShapeIndex = Random.Range(0, shapes.Length);
			Instantiate (shapes [randomShapeIndex], transform.position, Quaternion.identity);
		}
	}
}