﻿using System.Collections;
using UnityEngine;

namespace Controllers
{
	public class Grid : MonoBehaviour 
	{
		#region References
		public static Grid Instance;

		public delegate void GridRowsBlowEvent (int numRowsBlowed);	// return a number of consumed rows at the time
		public static event GridRowsBlowEvent RowsBlow;

		[SerializeField] int width;
		[SerializeField] int height;
		private Transform[,] grid;
		#endregion // References

		#region UnityCallbacks
		void Awake ()
		{
			Instance = this;
			grid = new Transform[width, height];
		}
		#endregion

		#region Events
		private void RowBlowAction (int nRowsBlowed)
		{
			if (RowsBlow != null)
			{
				RowsBlow (nRowsBlowed);
			}
		}
		#endregion

		#region Getters
		public Transform[,] GetGrid ()
		{
			return grid;
		}

		public int GetWidth ()
		{
			return width;
		}

		public int GetHeight ()
		{
			return height;
		}
		#endregion

		#region Helpers
		/// <summary>
		/// Rounds the Vector2 values, to be sure it's not spoiled by rotations.
		/// </summary>
		public Vector2 RoundVector2 (Vector2 v)
		{
			return new Vector2 (Mathf.Round (v.x), Mathf.Round (v.y));
		}

		/// <summary>
		/// Check the cell is on the field
		/// </summary>
		public bool IsInsideBorder (Vector2 position)
		{
			return ((int)position.x >= 0 && (int)position.x < width && (int)position.y >= 0);
		}

		/// <summary>
		/// Clear the row when it's full
		/// </summary>
		public void DeleteRow (int y)
		{
			for (int x = 0; x < width; ++x)
			{
				Destroy (grid [x, y].gameObject);
				grid [x, y] = null;
			}	
		}

		/// <summary>
		/// Put cells to the empty lower cells
		/// </summary>
		public void DecreaseRow (int y)
		{
			for (int x = 0; x < width; ++x)
			{
				if (grid [x, y] != null) 
				{
					// move upper not empty cell to the empty lower cell
					grid [x, y - 1] = grid [x, y];
					grid [x, y] = null;
					grid [x, y - 1].position += new Vector3 (0f, -1f, 0f);
				}
			}
		}

		/// <summary>
		/// Run through upper rows to put the cells down
		/// after the row (y) has been destroyed
		/// </summary>
		public void DecreaseUpperRows (int y)
		{
			for (int i = y; i < height; ++i) 
			{
				DecreaseRow (i);
			}
		}

		/// <summary>
		/// Check if (y) row is full
		/// </summary>
		public bool IsRowFull (int y)
		{
			for (int x = 0; x < width; ++x)
			{
				if (grid[x, y] == null)
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// check the grid for full rows and delete them
		/// and puts down full cells to lower empty positions
		/// </summary>
		public void DeleteFullRows ()
		{
			int blowedRowsCount = 0;

			for (int y = 0; y < height; ++y)
			{
				if (IsRowFull (y))
				{
					DeleteRow (y);
					DecreaseUpperRows (y + 1);
					--y;
					++blowedRowsCount;
				}
			}

			if (blowedRowsCount > 0)
			{
				RowBlowAction (blowedRowsCount);
			}
		}
		#endregion // helpers
	}
}