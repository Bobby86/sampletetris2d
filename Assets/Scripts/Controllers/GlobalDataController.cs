﻿using System.Collections;
using UnityEngine;

namespace Controllers
{
	public class GlobalDataController : MonoBehaviour 
	{
		#region References
		public delegate void GlobalDataEvents ();
		public static event GlobalDataEvents DifficultyUp;
		public static event GlobalDataEvents ScoreUp;
		public static event GlobalDataEvents LevelUp;
		public static GlobalDataController Instance;

		[SerializeField] int scoreToLvlUp = 300;
		[SerializeField] int scoreForSingleRow = 100;
		[SerializeField] float fallDelay = 1f;	// start fall delay value and a variable stores data for each new shape
		[SerializeField] float fallDelayReduction = 0.1f;
		[SerializeField] float minimumFallDelay = 0.2f;

		private int score;
		private int level;
		#endregion // References

		#region nity Callbacks
		void OnEnable ()
		{
			Grid.RowsBlow += RowsBlowTrigger;
		}

		void OnDisable ()
		{
			Grid.RowsBlow -= RowsBlowTrigger;
		}

		void Awake ()
		{
			Instance = this;
		}

		void Start ()
		{
			level = 1;
			score = 0;

			Invoke ("DifficultyUpEvent", 0.01f);
		}
		#endregion // unity callbacks

		#region Getters
		public float GetFallDelay ()
		{
			return fallDelay;
		}

		public int GetScore ()
		{
			return score;
		}

		public float GetLevel ()
		{
			return level;
		}
		#endregion

		#region Events and triggers
		private void DifficultyUpEvent ()
		{
			if (DifficultyUp != null)
			{
				DifficultyUp ();
			}
		}

		private void ScoreUpEvent ()
		{
			if (ScoreUp != null)
			{
				ScoreUp ();
			}
		}

		private void LevelUpEvent ()
		{
			if (LevelUp != null)
			{
				LevelUp ();
			}
		}

		// calls when row(s) has been consumed
		private void RowsBlowTrigger (int comboNumber)
		{
			if (comboNumber < 1)
			{
				comboNumber = 1;
			}

			if (comboNumber > 1)
			{	
				score += scoreForSingleRow * comboNumber + comboNumber * scoreForSingleRow / 4;	
			}
			else // comboNumber == 1
			{
				score += scoreForSingleRow;
			}

			if (score - scoreForSingleRow * level >= scoreToLvlUp)
			{
				++level;

				if (fallDelay > minimumFallDelay) 
				{	
					fallDelay -= fallDelayReduction;
					DifficultyUpEvent ();
				}
				LevelUpEvent ();
			}
			ScoreUpEvent ();
		}
		#endregion // Events
	}
}