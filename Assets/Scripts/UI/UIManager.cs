﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Controllers;

namespace UI
{
	public class UIManager : MonoBehaviour 
	{
		#region References
		[SerializeField] Text score;
		[SerializeField] Text level;
		[SerializeField] GameObject gameOverLabel;
		#endregion // References

		#region nity Callbacks
		void OnEnable ()
		{
			GlobalDataController.ScoreUp += ScoreUpTrigger;
			GlobalDataController.LevelUp += LevelUpTrigger;
			Shape.GameOverEvent += GameOverTrigger;
		}

		void OnDisable ()
		{
			GlobalDataController.ScoreUp -= ScoreUpTrigger;
			GlobalDataController.LevelUp -= LevelUpTrigger;
			Shape.GameOverEvent += GameOverTrigger;
		}

		void Start ()
		{
			score.text = "0";
			level.text = "0";
		}
		#endregion // unity callbacks

		#region Events and triggers
		private void ScoreUpTrigger ()
		{
			score.text = GlobalDataController.Instance.GetScore ().ToString ();
		}

		private void LevelUpTrigger ()
		{
			level.text = GlobalDataController.Instance.GetLevel ().ToString ();
		}

		private void GameOverTrigger ()
		{
			gameOverLabel.SetActive (true);
		}
		#endregion // Events
	}
}